FROM python:3.10.10-slim-bullseye

LABEL vendor1="DeusOps"
LABEL vendor2="Konstantin Deepezh aka D3pRe5s"

ENV ANSIBLE_VERSION=7.3.0

WORKDIR /opt

COPY ansible.cfg entrypoint.sh .

RUN apt-get update && apt-get install -y git openssh-client rsync gcc musl-dev \
    && pip3 install --no-cache-dir --upgrade pip ansible~=${ANSIBLE_VERSION} \
    && apt-get clean autoclean && apt-get autoremove --yes \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/ && python3 -m pip cache purge

RUN mkdir ~/.ssh && \
    ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts

ENTRYPOINT ["/bin/bash", "./entrypoint.sh"]
